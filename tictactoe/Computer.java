package tictactoe;

public class Computer {
	
	/**
	 * First checks if computer has 2 in a row, then if the player does.
	 * If not, checks for middle, corners and the rest.
	 * @param board  - current state of the board.
	 * @return cell index where computer makes its move.
	 */
	public int makeMove(int[] board){
		
		int cell = 0;
		int[] corners = {0, 2, 4, 8};		//corner indices
		int winningCell = winMove(board);
		
		if (winningCell > -1) {		// if winMove didn't find a winning move, returned -1
			cell = winningCell;
		}else{							/*If there was no two in a row/column/diagonal, starts looking 
		 								*in middle/corners and sides.*/
			if (board[4] == 0) {
				cell = 4;
				return cell;
			}
			for (int i = 0; i < corners.length; i++) {	//checks corners
				int boardIndex = corners[i];
				
				if(board[boardIndex] == 0){
					cell = boardIndex;
					return cell;
				}
			}
			for (int i = 0; i < board.length; i++) {	//checks whole gameboard
				if (board[i] == 0) {
					cell = i;
					//break;
					return cell;
				}
				
			}
		}
		
		return cell;
	}
	
	/**
	 * Checks if the gameboard has two of the same symbols in a row/column/diagonal.
	 * First checks for a winning move, then for blocking.
	 * @param board - current state of the board.
	 * @return winning index, if there is one. Otherwise 0.
	 */
	public int winMove(int[] board){
				// Computer's if-statements
				if (((board[1] == -1 && board[2] == -1)
						|| (board[4] == -1 && board[8] == -1) || (board[3] == -1 && board[6] == -1))
						&& board[0] == 0) {
					return 0;
				}
				if (((board[0] == -1 && board[2] == -1) || (board[4] == -1 && board[7] == -1))
						&& board[1] == 0) {
					return 1;
				}
				if (((board[0] == -1 && board[1] == -1)
						|| (board[4] == -1 && board[6] == -1) || (board[5] == -1 && board[8] == -1))
						&& board[2] == 0) {
					return 2;
				}
				if (((board[0] == -1 && board[6] == -1) || (board[4] == -1 && board[5] == -1))
						&& board[3] == 0) {
					return 3;
				}
				if (((board[0] == -1 && board[8] == -1)
						|| (board[2] == -1 && board[6] == -1)
						|| (board[1] == -1 && board[7] == -1) || (board[3] == -1 && board[5] == -1))
						&& board[4] == 0) { 
					return 4;
				}
				if (((board[3] == -1 && board[4] == -1) || (board[2] == -1 && board[8] == -1))
						&& board[5] == 0) { 
					return 5;
				}
				if (((board[0] == -1 && board[3] == -1)
						|| (board[2] == -1 && board[4] == -1) || (board[7] == -1 && board[8] == -1))
						&& board[6] == 0) {
					return 6;
				}
				if (((board[1] == -1 && board[4] == -1) || (board[6] == -1 && board[8] == -1))
						&& board[7] == 0) { 
					return 7;
				}
				if (((board[0] == -1 && board[4] == -1)
						|| (board[2] == -1 && board[5] == -1) || (board[6] == -1 && board[7] == -1))
						&& board[8] == 0) { 
					return 8;
				}
				// Player's if-statements
				if (((board[1] == 1 && board[2] == 1)
						|| (board[4] == 1 && board[8] == 1) || (board[3] == 1 && board[6] == 1))
						&& board[0] == 0) { 
					return 0;
				}
				if (((board[0] == 1 && board[2] == 1) || (board[4] == 1 && board[7] == 1))
						&& board[1] == 0) { 
					return 1;
				}
				if (((board[0] == 1 && board[1] == 1)
						|| (board[4] == 1 && board[6] == 1) || (board[5] == 1 && board[8] == 1))
						&& board[2] == 0) { 
					return 2;
				}
				if (((board[0] == 1 && board[6] == 1) || (board[4] == 1 && board[5] == 1))
						&& board[3] == 0) { 
					return 3;
				}
				if (((board[0] == 1 && board[8] == 1)
						|| (board[2] == 1 && board[6] == 1)
						|| (board[1] == 1 && board[7] == 1) || (board[3] == 1 && board[5] == 1))
						&& board[4] == 0) { 
					return 4;
				}
				if (((board[3] == 1 && board[4] == 1) || (board[2] == 1 && board[8] == 1))
						&& board[5] == 0) { 
					return 5;
				}
				if (((board[0] == 1 && board[3] == 1)
						|| (board[2] == 1 && board[4] == 1) || (board[7] == 1 && board[8] == 1))
						&& board[6] == 0) { 
					return 6;
				}
				if (((board[1] == 1 && board[4] == 1) || (board[6] == 1 && board[8] == 1))
						&& board[7] == 0) { 
					return 7;
				}
				if (((board[0] == 1 && board[4] == 1)
						|| (board[2] == 1 && board[5] == 1) || (board[6] == 1 && board[7] == 1))
						&& board[8] == 0) { 
					return 8;
				}

				return -1;		//when nothing found previously
	}
	
	
	
}
