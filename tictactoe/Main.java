package tictactoe;

public class Main {

	public static void main(String[] args) {
		UserInput userInput = new UserInput();
		Board printBoard = new Board();
		GameControl gameControl = new GameControl();
		Computer computer = new Computer();
		
		boolean isPlayerTurn = true;
		
		printBoard.printEmptyBoard();
		
		System.out.println("Enter number 1-9 to play");
		
		while(gameControl.isGameOver(printBoard.board) == false && gameControl.checkWin(printBoard.board) == 0){
			
			if(isPlayerTurn == true){
				int insertedNr = userInput.readInput();
				if(printBoard.board[insertedNr - 1] == 0) {
					printBoard.board[insertedNr - 1] = 1;
					isPlayerTurn = false;
					printBoard.printBoard(printBoard.board);	
				}else{
					System.out.println("Can't move there");
				}
				
			}else{
				System.out.println("Computer move: ");
				int computerNr = computer.makeMove(printBoard.board);
				printBoard.board[computerNr] = -1;
				isPlayerTurn = true;
				printBoard.printBoard(printBoard.board);
			}
		}
			System.out.println("Game over!");
			
			int winner = gameControl.checkWin(printBoard.board);
			
			if(winner == 1){
				System.out.println("You won!");
			}else if(winner == -1){
				System.out.println("Computer won!");
			}else{
				System.out.println("It's a draw!");
			}
			
	}
	
}