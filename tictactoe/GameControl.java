package tictactoe;

public class GameControl {
	
	/**
	 * Checks if the game has ended.
	 * @param board - current state of the board.
	 * @return true if there are no free spots, otherwise false.
	 */
	public boolean isGameOver(int[] board){
		
		for (int i = 0; i < board.length; i++) {
			if (board[i] == 0) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Checks the board and returns which player has a winning combination.
	 * @param board - current state of the board.
	 * @return Indicator of the player who has the winning combination. If no one wins, returns 0.
	 */
	public int checkWin(int[] board) {
		// rows
		if (board[0] == board[1] && board[1] == board[2]) {
			return board[0];
		}
		if (board[3] == board[4] && board[4] == board[5]) {
			return board[3];
		}
		if (board[6] == board[7] && board[7] == board[8]) {
			return board[6];
		}
		// columns
		if (board[0] == board[3] && board[3] == board[6]) {
			return board[0];
		}
		if (board[1] == board[4] && board[4] == board[7]) {
			return board[1];
		}
		if (board[2] == board[5] && board[5] == board[8]) {
			return board[2];
		}
		// diagonals
		if (board[0] == board[4] && board[4] == board[8]) {
			return board[0];
		}
		if (board[2] == board[4] && board[4] == board[6]) {
			return board[2];
		}
		return 0;
	}
	
	
	
}
