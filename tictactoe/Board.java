package tictactoe;

public class Board {
	
	public int board[] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
	
	/**
	 * Prints out an empty gameboard.
	 */
	public void printEmptyBoard(){
		System.out.println("+---+---+---+");
		System.out.println("| 1 | 2 | 3 |");
		System.out.println("+---+---+---+");
		System.out.println("| 4 | 5 | 6 |");
		System.out.println("+---+---+---+");
		System.out.println("| 7 | 8 | 9 |");
		System.out.println("+---+---+---+ \n");
	}
	
	/**
	 * Prints X-s and O-s to mark player's and computer's moves accordingly.
	 * @param board - current state of the board.
	 */
	public void printBoard(int[] board){
		
		for (int i = 0; i < board.length; i++) {
			
			String symbol = "";
			
			if (board[i] == 1) {
				symbol = "X";
			}else if (board[i] == -1) {
				symbol = "O";
			}else{
				symbol = " ";
			}
			
			if (i % 3 == 0) {
				System.out.print("\n+---+---+---+");
				System.out.print("\n| " + symbol + " | ");
			}else{
				System.out.print(symbol + " | ");
			}	
		}
		
		System.out.println("\n+---+---+---+");
		
	}
	
}
