package tictactoe;

import lib.TextIO;

public class UserInput {

	/**
	 * @return user's input.
	 */
	public int readInput() {
		int nr = 0;
		boolean isOK = false;

		System.out.println("Your move:");

		while (isOK == false) {

			nr = TextIO.getlnInt();

			if (nr > 0 && nr < 10 && nr == (int) nr) { // x == (int)x -->
														// http://stackoverflow.com/questions/5502548/checking-if-a-number-is-an-integer-in-java
				isOK = true;
			} else {
				System.out.println("Try again");
				readInput();
			}

		}

		return nr;
	}
}
